#+begin_src sh :mkdirp yes :tangle ~/.zshrc
  # Setup package manager
  [[ -d ~/.zinit ]] || git clone https://github.com/zdharma/zinit.git ~/.zinit/bin
  source ~/.zinit/bin/zinit.zsh

  # Setup theme
  zinit ice compile'(pure|async).zsh' pick'async.zsh' src'pure.zsh'
  zinit load sindresorhus/pure

  # Setup better autocompletion
  zinit load zsh-users/zsh-completions

  # Setup autosuggestions
  zinit load zsh-users/zsh-autosuggestions

  # Search history for matching command
  zinit load zsh-users/zsh-history-substring-search
  bindkey '^[[A' history-substring-search-up
  bindkey '^[[B' history-substring-search-down
  bindkey -M emacs '^P' history-substring-search-up
  bindkey -M emacs '^N' history-substring-search-down

  # Enable fzf autocompletion and keymap
  # source $(brew --prefix fzf)/shell/completion.zsh
  # source $(brew --prefix fzf)/shell/key-bindings.zsh

  # Enable autocompletion
  autoload -Uz compinit && compinit -i

  # Enable syntax highlighting
  zinit load zsh-users/zsh-syntax-highlighting

  # Configure path
  typeset -U PATH path
  # path=($(brew --prefix)/bin $(brew --prefix)/sbin $path)
  export PATH

  # Start keychain
  # eval `keychain --eval id_rsa id_ed25519`

  # History settings
  HISTFILE=~/.zhistory
  HISTSIZE=1000000
  SAVEHIST=1000000
  # Append session history to history file
  setopt append_history
  # Share history accross concurrently active sessions
  setopt share_history

  # Configure terminal and gui editors
  EDITOR='emacsclient -t -a='
  VISUAL='emacsclient -cn -a='
  alias e=$EDITOR
  alias v=$VISUAL
#+end_src

#+begin_src conf-unix :mkdirp yes :tangle ~/.gitconfig
  # Configure user details
  [user]
    name = Shreyanshu Shekhar
    email = shreyanshushekhar.ss@gmail.com
  [github]
    user = ShreyanshuShekhar

  [alias]
    # Delete all local branches tracking squashed remote branches
    delete-squashed = git checkout master && git remote prune origin | rg pruned | cut -d / -f 2 | xargs git branch -D
#+end_src

#+begin_src conf-colon :mkdirp yes :tangle ~/.config/gh/config.yml
  # Use ssh for clone and push operations
  git_protocol: ssh

  aliases:
    # Clone all repositories for an organization
    orgc: '!gh api --paginate orgs/"$1"/repos | jq ".[] | .ssh_url" | xargs -P 4 -n 1 git clone'
#+end_src

#+begin_src emacs-lisp :mkdirp yes :tangle ~/.emacs.d/init.el
  ;; Maxmize windoow on startup
  (setq default-frame-alist '((fullscreen . maximized)))

  ;; Setup package manager
  (setq straight-use-package-by-default t)
  (defvar bootstrap-version)
  (let ((bootstrap-file
	 (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
	(bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
	  (url-retrieve-synchronously
	   "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	   'silent 'inhibit-cookies)
	(goto-char (point-max))
	(eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))
  (straight-use-package 'use-package)

  ;; Setup theme
  (use-package doom-themes
    :config
    (load-theme 'doom-one t))

  ;; Support system package installation
  (use-package system-packages)
  (use-package helm-system-packages)

  ;; Setup git interface
  (use-package magit
    :bind
    ("C-x C-g" . magit-status))

  ;; Setup github and gitlab helpers
  (use-package forge
    :after magit)

  ;; Setup incremental completion
  (use-package helm
    :config
    (setq helm-split-window-inside-p t)
    (helm-mode)
    :bind
    (("M-x" . helm-M-x)
     ("C-x C-f" . helm-find-files)
     ("C-x C-b" . helm-mini)))

  ;; Setup project management
  (use-package projectile
    :config
    (projectile-mode)
    :bind-keymap
    ("s-p" . projectile-command-map))
  (use-package helm-projectile
    :after helm projectile
    :config
    (helm-projectile-on))

  ;; Setup snippet support
  (use-package yasnippet
    :config
    (yas-global-mode))
  (use-package yasnippet-snippets
    :after yasnippet)

  ;; Setup autocompletion
  (use-package company
    :config
    (setq company-idle-delay 0
	  company-minimum-prefix-length 1)
    (add-to-list 'company-backends '(company-yasnippet company-dabbrev-code company-capf))
    (global-company-mode))
  (use-package company-statistics
    :after company
    :config
    (company-statistics-mode))
  (use-package company-shell
    :config
    (add-to-list 'company-backends '(company-shell company-shell-env)))

  ;; Setup api client
  (use-package restclient)

  ;; Setup vim emulation
  (use-package evil
    :config
    (evil-mode))
  (use-package undo-fu
    :after evil
    :bind
    (:map evil-normal-state-map
	  ("u" . undo-fu-only-undo)
	  ("C-r" . undo-fu-only-redo)))
  (use-package evil-surround
    :after evil
    :config
    (global-evil-surround-mode))
  (use-package evil-matchit
    :after evil
    :config
    (global-evil-matchit-mode))
  (use-package evil-commentary
    :after evil
    :config
    (evil-commentary-mode))

  ;; Sync emacs path and shell path
  (use-package exec-path-from-shell
    :config
    (exec-path-from-shell-initialize))

  ;; Cache keys via keychain
  ;; (use-package keychain-environment
  ;;   :config
  ;;   (keychain-refresh-environment))

  ;; Let emacs query passphrases through minibuffer
  (setq epg-pinentry-mode 'loopback)

  ;; Do not expire cached passwords at all
  (setq password-cache-expiry nil)

  ;; Show line numbers
  (global-linum-mode)

  ;; Hide toolbar
  (tool-bar-mode 0)

  ;; Hide scrollbar
  (scroll-bar-mode 0)

  ;; Do not show welcome screen on startup
  (setq inhibit-startup-screen t)

  ;; Do not show scratch buffer message
  (setq initial-scratch-message nil)

  ;; Autocomplete pairs
  (electric-pair-mode)

  ;; Show matching pair instantly
  (setq show-paren-delay 0)
  (show-paren-mode)

  ;; Highlight current line
  (global-hl-line-mode)

  ;; Disable cursor blinking
  (blink-cursor-mode 0)

  ;; Configure user details
  (setq user-full-name "Shreyanshu Shekhar"
	user-mail-address "shreyanshushekhar.ss@gmail.com"
	epa-file-encrypt-to user-mail-address)

  ;; Track recent files
  (recentf-mode)

  ;; Enable acronym completion
  (add-to-list 'completion-styles 'initials 'append)


  ;; Enable fuzzy completion
  (add-to-list 'completion-styles 'flex 'append)

  ;; Set emacs starting directory
  (setq default-directory "~/")

  ;; Enable single char input for yes and no
  (defalias 'yes-or-no-p 'y-or-n-p)

  ;; Do not litter backup files everywhere
  (setq backup-directory-alist `(("." . ,(concat user-emacs-directory "backups"))))

  ;; Autosave files
  (setq auto-save-visited-interval 0)
  (auto-save-visited-mode)

  ;; Start emacs server if not running already
  (use-package server
    :config
    (unless (server-running-p)
      (server-start)))

  ;; Tangle dotfiles on save
  (add-hook 'after-save-hook '(lambda ()
				(when (and (string= default-directory (expand-file-name "~/dotfiles/"))
					   (string-match-p ".org$" buffer-file-name))
				  (org-babel-tangle-file buffer-file-name))))

  ;; Tangle all dotfiles
  (defun tangle-dotfiles ()
    "Tangle all dotfiles"
    (interactive)
    (dolist (file (directory-files-recursively "~/dotfiles" ".org$"))
      (org-babel-tangle-file file)))
#+end_src

#+begin_src snippet :mkdirp yes :tangle ~/.emacs.d/snippets/emacs-lisp-mode/straight-use-package
  # -*- mode: snippet -*-
  # name: straight-use-package 
  # key: sup
  # --
  (use-package ${1:package-name})
#+end_src
